<?php

/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 25/05/2016
 * Time: 02:26
 */
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GrowBotBundle\Entity\Sensor;
use GrowBotBundle\Entity\Sensordata;
use GrowBotBundle\Entity\Sensortype;

class LoadSensordataData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $sensortype1 = new Sensortype( "Temperature" );
        $sensortype2 = new Sensortype( "Mist" );
        
        $sensor1 = new Sensor( "Sensor 1" );
        $sensor1->setSensorType( $sensortype2 );
        $sensor2 = new Sensor( "Sensor 2" );
        $sensor2->setSensorType( $sensortype1 );

        $sensordata1 = Sensordata::withSensorAndValue( $sensor1, 32 );
        $sensordata2 = Sensordata::withSensorAndValue( $sensor2, 24 );

        $manager->persist( $sensordata1 );
        $manager->persist( $sensordata2 );

        $manager->flush();
    }
}