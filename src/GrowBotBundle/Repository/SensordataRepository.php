<?php

namespace GrowBotBundle\Repository;

/**
 * SensordataRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SensordataRepository extends \Doctrine\ORM\EntityRepository
{
    public function getPastData( $sensorsIdArray, $hours ) {

        $datetime24 = date( "Y-m-d H:i:s", strtotime( "-$hours hours" ) );

        $qb = $this->createQueryBuilder( 'sd' );
        
        $qb
            ->where( 'sd.datetime > :dat' )
            ->andWhere($qb->expr()->in( 'sd.sensor', $sensorsIdArray ))
            ->setParameter( 'dat', $datetime24 )
        ;

        return $qb
            ->getQuery()
            ->getResult()
            ;
    }

    public function getLastInsert( $sensorsIdArray ){

        $qb = $this->createQueryBuilder( 'sd' );

        $qb
            ->where($qb->expr()->in( 'sd.sensor', $sensorsIdArray ))
            ->orderBy('sd.datetime', 'DESC')
            ->setMaxResults( sizeof($sensorsIdArray) )
        ;

        return $qb
            ->getQuery()
            ->getResult()
            ;
    }

}
