<?php

namespace GrowBotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sensor
 *
 * @ORM\Table(name="sensor")
 * @ORM\Entity(repositoryClass="GrowBotBundle\Repository\SensorRepository")
 */
class Sensor
{
    /**
     * Sensor constructor.
     * @param $sensorname
     */
    public function __construct( $sensorname )
    {
        // Par défaut, la date de l'annonce est la date d'aujourd'hui
        $this->setName($sensorname);
    }
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="GrowBotBundle\Entity\Sensortype",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $sensortype;

    /**
     * @var string
     *
     * @ORM\Column(name="more", type="string", length=255, nullable=true)
     */
    private $more;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sensor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param Sensortype $type
     *
     * @return Sensor
     */
    public function setSensorType($typ)
    {
        $this->sensortype = $typ;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getSensorType()
    {
        return $this->sensortype;
    }

    /**
     * Set more
     *
     * @param string $more
     *
     * @return Sensor
     */
    public function setMore($more)
    {
        $this->more = $more;

        return $this;
    }

    /**
     * Get more
     *
     * @return string
     */
    public function getMore()
    {
        return $this->more;
    }
}

