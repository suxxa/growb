<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 02/06/2016
 * Time: 15:46
 */

namespace GrowBotBundle\Entity;

use Symfony\Component\Yaml\Yaml;


/**
 * Class YMLChartColorsLoader
 * @package GrowBotBundle\Entity
 */
class YMLChartColorsLoader
{
    /**
     * @var array
     */
    private $chartColorsArray = array();

    /**
     * YMLChartColorsLoader constructor.
     */
    public function __construct() {

        $ymlDir = explode( "\\E", __DIR__ );

        $values = Yaml::parse( file_get_contents( $ymlDir[0] . "\\Resources\\config\\chartColors.yml" ) );

        foreach ( $values as $key=>$val ) {
            $tmpChartColor = new ChartColor();
            $tmpChartColor->load( $val );
            $this->chartColorsArray[$key] = $tmpChartColor;
        }

    }

    /**
     * @return array
     */
    public function getChartColorsArray()
    {
        return $this->chartColorsArray;
    }

    /**
     * @param array $chartColorsArray
     */
    public function setChartColorsArray($chartColorsArray)
    {
        $this->chartColorsArray = $chartColorsArray;
    }

    /**
     * @param $idColor
     * @return mixed
     */
    public function getColorChart($idColor ){
        return $this->getChartColorsArray()[ $idColor ];
    }
    
}
