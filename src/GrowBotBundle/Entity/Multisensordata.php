<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 26/05/2016
 * Time: 00:40
 */

namespace GrowBotBundle\Entity;


/**
 * Class Multisensordata
 * @package GrowBotBundle\Entity
 */
class Multisensordata
{
    /**
     * @var
     */
    private $dataArray;

    /**
     * @param $dataArray
     * @return Multisensordata
     */
    public static function initWithDataArray($dataArray ){
        $self = new self();
        $self->dataArray = $dataArray;
        return $self;
    }


    /**
     * @return mixed
     */
    public function generateAndGetJSON( ){

        $labelsArray = array();
        $valuesArray = array();
        $returnArray = array();
        $returnArray["labels"] = array();

        foreach( $this->dataArray as $dt ){

            if( !in_array( $dt->getDatetime()->format('d-m-Y H:i:s'), $labelsArray ) ) {
                array_push( $labelsArray, $dt->getDatetime()->format('d-m-Y H:i:s') );
            }

            $valuesArray[ $dt->getSensor()->getSensortype()->getName()  ][] = $dt->getValue();

        }
        
        array_push( $returnArray["labels"], $labelsArray );

        foreach ( $valuesArray as $key => $arr ){

            $tmpArray = array();
            $tmpArray["label"] = array();
            $tmpArray["data"] = array();

            $tmpArray["label"] = $key ;

            foreach ( $arr as $val ){
                array_push( $tmpArray["data"], $val );
            }

            $returnArray["datasets"][] = $tmpArray;

        }

        $chartColorsArray = new YMLChartColorsLoader();

        $finalArray = array();
        $finalArray["labels"] = array();
        $finalArray["datasets"] = array();


        array_push( $finalArray["labels"], $returnArray["labels"] );

        foreach ( $returnArray["datasets"] as $dataset ) {
            if ( $chartCol = $chartColorsArray->getColorChart( $dataset["label"] ) ){

                $dataset["fillColor"] = $chartCol->getFillColor();
                $dataset["strokeColor"] = $chartCol->getStrokeColor();
                $dataset["pointColor"] = $chartCol->getPointColor();
                $dataset["pointStrokeColor"] = $chartCol->getPointStrokeColor();
                $dataset["pointHighlightFill"] = $chartCol->getPointHighlightFill();
                $dataset["pointHighlightStroke"] = $chartCol->getPointHighlightStroke();

                array_push( $finalArray["datasets"], $dataset );
            }
        }

        return json_encode( $finalArray );
    }

    /**
     * @return mixed
     */
    public function getDataArray()
    {
        return $this->dataArray;
    }

    /**
     * @param mixed $dataArray
     */
    public function setDataArray($dataArray)
    {
        $this->dataArray = $dataArray;
    }

}
