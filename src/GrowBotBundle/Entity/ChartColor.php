<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 02/06/2016
 * Time: 15:48
 */

namespace GrowBotBundle\Entity;


/**
 * Class ChartColor
 * @package GrowBotBundle\Entity
 */
class ChartColor
{
    /**
     * @var
     */
    private $fillColor;
    /**
     * @var
     */
    private $strokeColor;
    /**
     * @var
     */
    private $pointColor;
    /**
     * @var
     */
    private $pointStrokeColor;
    /**
     * @var
     */
    private $pointHighlightFill;
    /**
     * @var
     */
    private $pointHighlightStroke;

    /**
     * @param $valArray
     */
    public function load( $valArray ){

        $this->setFillColor( $valArray["fillColor"] );
        $this->setStrokeColor( $valArray["strokeColor"] );
        $this->setPointColor( $valArray["pointColor"] );
        $this->setPointStrokeColor( $valArray["pointStrokeColor"] );
        $this->setPointHighlightFill( $valArray["pointHighlightFill"] );
        $this->setPointHighlightStroke( $valArray["pointHighlightStroke"] );

    }

    /**
     * @return mixed
     */
    public function getFillColor()
    {
        return $this->fillColor;
    }

    /**
     * @param mixed $fillColor
     */
    public function setFillColor($fillColor)
    {
        $this->fillColor = $fillColor;
    }

    /**
     * @return mixed
     */
    public function getStrokeColor()
    {
        return $this->strokeColor;
    }

    /**
     * @param mixed $strokeColor
     */
    public function setStrokeColor($strokeColor)
    {
        $this->strokeColor = $strokeColor;
    }

    /**
     * @return mixed
     */
    public function getPointColor()
    {
        return $this->pointColor;
    }

    /**
     * @param mixed $pointColor
     */
    public function setPointColor($pointColor)
    {
        $this->pointColor = $pointColor;
    }

    /**
     * @return mixed
     */
    public function getPointStrokeColor()
    {
        return $this->pointStrokeColor;
    }

    /**
     * @param mixed $pointStrokeColor
     */
    public function setPointStrokeColor($pointStrokeColor)
    {
        $this->pointStrokeColor = $pointStrokeColor;
    }

    /**
     * @return mixed
     */
    public function getPointHighlightFill()
    {
        return $this->pointHighlightFill;
    }

    /**
     * @param mixed $pointHighlightFill
     */
    public function setPointHighlightFill($pointHighlightFill)
    {
        $this->pointHighlightFill = $pointHighlightFill;
    }

    /**
     * @return mixed
     */
    public function getPointHighlightStroke()
    {
        return $this->pointHighlightStroke;
    }

    /**
     * @param mixed $pointHighlightStroke
     */
    public function setPointHighlightStroke($pointHighlightStroke)
    {
        $this->pointHighlightStroke = $pointHighlightStroke;
    }

}
