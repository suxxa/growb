<?php

namespace GrowBotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sensordata
 *
 * @ORM\Table(name="sensordata")
 * @ORM\Entity(repositoryClass="GrowBotBundle\Repository\SensordataRepository")
 */
class Sensordata
{

    public function __construct()
    {
        // Par défaut, la date de l'annonce est la date d'aujourd'hui
        $this->setDatetime(new \Datetime());
    }

    public static function withSensorAndValue( $sensor, $val ) {
        $self = new self();
        $self->setSensor( $sensor );
        $self->setValue( $val );
        return $self;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity="GrowBotBundle\Entity\Sensor",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $sensor;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="smallint")
     */
    private $value;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Sensordata
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set sensor
     *
     * @param Sensor $sensor
     *
     * @return Sensordata
     */
    public function setSensor($sensor)
    {
        $this->sensor = $sensor;

        return $this;
    }

    /**
     * Get sensor
     *
     * @return string
     */
    public function getSensor()
    {
        return $this->sensor;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return Sensordata
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }
}

