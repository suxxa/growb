<?php

namespace GrowBotBundle\Controller;

use GrowBotBundle\Entity\Multisensordata;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DashboardController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $repo = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('GrowBotBundle:Sensordata');

        $sensors = array( 11, 12 );
        $res = $repo->getPastData( $sensors, 24 );

        $resDos = $repo->getLastInsert( $sensors );

        $dtarr = Multisensordata::initWithDataArray($res);

        return $this->render('GrowBotBundle:Dashboard:index.html.twig', array(
            'sdata' => $dtarr->generateAndGetJSON(),
            ));
    }
}
