<?php

namespace GrowBotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class OptionsController extends Controller
{
    /**
     * @Route("/options")
     */
    public function indexAction()
    {
        return $this->render('GrowBotBundle:Options:index.html.twig');
    }
}
